<?php
declare(strict_types=1);

namespace Mastering\StoreInformation\ViewModel;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Model for view pages
 */
class StoreInformationViewModel implements ArgumentInterface
{

    private const PHONE_CONFIG = 'general/store_information/phone';
    private const STORE_NAME_CONFIG = 'general/store_information/name';
    private const STORE_HOURS_CONFIG = 'general/store_information/hours';
    private const COUNTRY_ID_CONFIG = 'general/store_information/country_id';
    private const REGION_ID_CONFIG = 'general/store_information/region_id';
    private const POSTCODE_CONFIG = 'general/store_information/postcode';
    private const CITY_CONFIG = 'general/store_information/city';


    private \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ){
        $this->scopeConfig = $scopeConfig;
    }

    public function getPhoneNumber()
    {
        return $this->scopeConfig->getValue(
            self::PHONE_CONFIG,
            ScopeInterface::SCOPE_STORE
        );
    }

    public function getStoreName()
    {
        return $this->scopeConfig->getValue(
            self::STORE_NAME_CONFIG,
            ScopeInterface::SCOPE_STORE
        );
    }

    public function getStoreHours()
    {
        return $this->scopeConfig->getValue(
            self::STORE_HOURS_CONFIG,
            ScopeInterface::SCOPE_STORE
        );
    }

    public function getCountryId()
    {
        return $this->scopeConfig->getValue(
            self::COUNTRY_ID_CONFIG,
            ScopeInterface::SCOPE_STORE
        );
    }

    public function getRegionId()
    {
        return $this->scopeConfig->getValue(
            self::REGION_ID_CONFIG,
            ScopeInterface::SCOPE_STORE
        );
    }

    public function getPostcode()
    {
        return $this->scopeConfig->getValue(
            self::POSTCODE_CONFIG,
            ScopeInterface::SCOPE_STORE
        );
    }

    public function getCity()
    {
        return $this->scopeConfig->getValue(
            self::CITY_CONFIG,
            ScopeInterface::SCOPE_STORE
        );
    }
}
